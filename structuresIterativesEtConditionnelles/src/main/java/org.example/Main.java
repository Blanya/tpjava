package org.example;


import org.example.decalage.DecalageTableau;
import org.example.photocopie.Photocopie;
import org.example.randomArray.RandomArray;
import org.example.renduMonnaie.RenduMonnaie;
import org.example.tableauNotes.TableauNotes;

public class Main {
    public static void main(String[] args) {
//        DecalageTableau.getDecalage();
//        Photocopie.getPhotocopiePrice();
//        RandomArray.askLong();
//        RenduMonnaie.getMonnaie();
        TableauNotes.completeArray();
    }
}