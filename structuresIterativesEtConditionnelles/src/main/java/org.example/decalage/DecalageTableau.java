package org.example.decalage;

import java.util.Arrays;

public class DecalageTableau{

    public static void getDecalage()
    {
        char[] table = {'D', 'E', 'C', 'A', 'L', 'A', 'G', 'E'};

        char temp;

        System.out.println("Tableau avant traitement : " + Arrays.toString(table));

        for (int i = 0; i < table.length-1; i++) {
            temp = table[i];
            table[i] = table[i+1];
            table[i+1] = temp;
        }

        System.out.println(Arrays.toString(table));
    }
}
