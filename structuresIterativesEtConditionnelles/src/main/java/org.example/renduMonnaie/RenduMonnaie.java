package org.example.renduMonnaie;

import java.util.Scanner;

import static java.lang.Integer.parseInt;

public class RenduMonnaie {

    public static void getMonnaie() {
        int achats = 0;

        Scanner sc = new Scanner(System.in);

        System.out.println("Je vais prendre le montant de vos articles (entier finissant par 0), si vous n'en avez plus, tapez exit");
        String choice;
        do {
            System.out.println("Donnez moi le prix de votre article : ");
            choice = sc.next();
            try
            {
                int value = parseInt(choice);

                if((value%10) != 0)
                {
                    System.out.println("Prix incorrect, donnez moi le prix de votre article : ");
                    choice = sc.next();
                }
                else{
                    achats += value;
                }
            }catch(NumberFormatException e) {
                if(choice.equals("exit"))
                    break;
                else
                    System.out.println("Cette valeur n'est pas un chiffre, essaie encore !");

            }
        }while(!choice.equals("exit"));

        System.out.printf("Vous devez payer %d euros, combien me donnez-vous? ", achats);

        int sommeDonnee = sc.nextInt();

        while (sommeDonnee < achats)
        {
            System.out.println("Vous ne m'avez pas donné assez, vous m'avez donné " + sommeDonnee + " euros. Veuillez ajouter de l'argent svp");
            int erreur = sc.nextInt();
            sommeDonnee = sommeDonnee + erreur;
        }

        int sommeDue = sommeDonnee - achats;
        System.out.println("Je vous dois " + sommeDue + " euros" );

        while(sommeDue>=10)
        {
            sommeDue -= 10;
            System.out.println("10 euros");
        }
        while(sommeDue>=5)
        {
            sommeDue -= 5;
            System.out.println("5 euros");
        }
        while(sommeDue>=1)
        {
            sommeDue -= 1;
            System.out.println("1 euros");
        }
    }
}
