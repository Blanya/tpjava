package org.example.photocopie;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Photocopie {
    public static void getPhotocopiePrice() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Entrez le nombre de photocopies a effectuer");
        int choice = sc.nextInt();

        float price = 0;
        String unites = "centimes";

        if (choice < 10) {
            price = choice * 15;
        } else if (choice <= 20) {
            price = choice * 10;
        } else {
            price = choice * 5;
        }

        if (price >= 100){
            price = price / 100;
            unites = "euros";
        }

        DecimalFormat double_decimal = new DecimalFormat("#.##");

        System.out.printf("Vous devez payer %s %s pour vos %d photocopie(s)", double_decimal.format(price), unites, choice);
    }
}
