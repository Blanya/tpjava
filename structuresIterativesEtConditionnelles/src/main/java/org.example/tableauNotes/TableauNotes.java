package org.example.tableauNotes;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;


public class TableauNotes {
    public static void completeArray()
    {
        Scanner sc = new Scanner(System.in);
        String answer;
        float note;
        float[] table = new float[20];

        int i = 0;
        do {
            System.out.printf("Donnez moi la note n°%d à entrer dans le tableau ", i+1);
            answer = sc.next();
            try
            {
                note = Float.parseFloat(answer);

                if(note<=20 && note>=0)
                {
                    table[i] = note;
                    i++;
                }

            }catch (NumberFormatException e)
            {
                System.out.printf("Saisie incorrecte, donnez moi la note n°%d à entrer dans le tableau ", i+1);
            }
        } while(i!=20);

        System.out.println("Voici votre tableau de notes : " + Arrays.toString(table));

        float somme = 0;

        for (float number : table) {
            somme += number;
        }
        float moyenne = somme / table.length;

        DecimalFormat double_decimal = new DecimalFormat("#.##");
        System.out.println("La moyenne de la classe est de " + double_decimal.format(moyenne));

        double min = table[0];
        double max = table[0];

        for (int j = 1; j < table.length ; j++) {
            if(table[j]<min)
            {
                min = table[j];
            }
            if(table[j]>max)
            {
                max = table[j];
            }
        }

        System.out.printf("La plus petite note de la classe est %s et la plus élevée est de %s", double_decimal.format(min), double_decimal.format(max));
    }
}