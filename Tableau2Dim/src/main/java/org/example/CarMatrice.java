package org.example;

import java.util.Arrays;
import java.util.Scanner;

public class CarMatrice {
    static Scanner sc = new Scanner(System.in);

    public static void addCarAndSeller()
    {
        System.out.println("Combien de vendeurs y-a-t-il? ");
        int sellers = sc.nextInt();

        System.out.println("Combien de véhicules différents sont à vendre? ");
        int cars = sc.nextInt();

        addNumbersCars(sellers, cars);
    }

    public static void addNumbersCars(int line, int column)
    {
        String matrice[][] = new String[line+1][column+1];

        for (int i = 1; i <= line ; i++) {
            System.out.println("Donnez moi le nom du vendeur n° " + i);
            matrice[i][0] = sc.next();
        }
        for (int i = 1; i <= column ; i++) {
            System.out.println("Donnez moi le nom de la voiture n° " + i);
            matrice[0][i] = sc.next();
        }

        for (int i = 1; i <= line; i++) {
            for(int j = 1; j <= column; j++)
            {
                String name = matrice[i][0];
                String car = matrice[0][j];
                System.out.printf("Combien de %s a vendu %s ? ", car, name);
                matrice[i][j] = sc.next();
            }
            System.out.println();
        }
        System.out.println(Arrays.deepToString(matrice));
    }
}
