package org.example;

public class MaxMatrice {
    public static void getMaxMatrice(){
        int matrice[][] = {
                {45, 23, 34, 56, 30},
                {67, 75, 21, 52, 59},
                {89, 34, 19, 19, 15},
                {1, 78, 90, 48, 42}
        };

        int line = matrice.length;
        int column = matrice[0].length;

        int max = matrice[0][0];

        for (int i = 0; i < line; i++)
        {
            for(int j = 0; j< column; j++) {
                if (matrice[i][j] > max)
                    max = matrice[i][j];
                System.out.println(matrice[i][j]);
            }
        }

        System.out.println("La valeur la plus élevée de la matrice est : " + max);
    }
}
