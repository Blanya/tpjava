package org.example;

import java.util.Arrays;
import java.util.Scanner;

public class AddMatrice {
    public static void askAndAddNumbers()
    {
        Scanner sc = new Scanner(System.in);

        int line;
        int column;

        do {
            System.out.println("Donnez moi le nombre de lignes de votre tableau");
            line = sc.nextInt();

            System.out.println("Donnez moi le nombre de colonnes de votre tableau");
            column = sc.nextInt();
        }while(column<line);


        int matrice[][] = new int[line][column];

        int count = 0;
        int sum = 0;
        long product = 1;

        for (int i = 0; i < line; i++) {
            for (int j = 0; j < column; j++) {
                count++;
                matrice[i][j] = count;
                sum += count;
                product *= count;
            }
        }

        float moy = sum/count ;

        System.out.println("Voici votre tableau généré " + Arrays.deepToString(matrice));
        System.out.println("La somme de toutes les valeurs du tableau est de " + sum);
        System.out.println("Le produit de toutes les valeurs du tableau est de " + product);
        System.out.println("La moyenne de toutes les valeurs du tableau est de " + moy);
    }
}
