package org.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class FilterWords {
    public static void askWords()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Entrez moi des mots séparés par un espace");
        String answer = sc.nextLine();

        System.out.println("Quelle taille minimale doivent avoir les mots ? ");
        int nb = sc.nextInt();

        String[] table = answer.split(" ");

//        ArrayList<String> filteredWord = filterWordsByLength(nb, table);
//        System.out.println("Voici vos mots avec la longueur minimale de " + nb + " : " + filteredWord);

        List<String> filteredWord = filterWordsByLengthStream(nb, table);

    }

    public static ArrayList<String> filterWordsByLength(int minLength, String[] mots) {

        ArrayList<String> filterTable = new ArrayList<String>();

        for (String word: mots) {
            if(word.length()>= minLength)
            {
                filterTable.add(word);
            }
        }

       return filterTable;
    }

    public static List<String> filterWordsByLengthStream(int minLength, String[] mots)
    {
        List<String> filterTable = Arrays.stream(mots)
                .filter(mot -> mot.length() >= minLength)
                .collect(Collectors.toList());

        System.out.println("Voici vos mots avec une longueur minimale de " + minLength);

        filterTable.forEach(System.out::println);

        return filterTable;
    }
}
