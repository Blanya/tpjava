package org.example.choice;

import java.util.Scanner;

public class SwitchClass {
    public static void askChoice()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Bonjour que souhaitez-vous faire ?");
        System.out.println("1. Login");
        System.out.println("2. Sign_Up");
        System.out.println("3. Terminate_Program");
        System.out.println("4. Main_Menu");
        System.out.println("5. About_App");

        String choice = sc.next();
        choice = choice.toLowerCase();

        switch (choice)
        {
            case "login":
                System.out.println("Please, enter your username");
                break;
            case "sign_up":
                System.out.println("Welcome!");
                break;
            case "terminate_program":
                System.out.println("Thank you ! GoodBye!");
                break;
            case "main_menu":
                System.out.println("Main menu");
                break;
            case "about_app":
                System.out.println("This App is created by me with some supports");
                break;
            default:
                System.out.println("Please enter one of other values: login, sign_up, terminate_program, main_menu, about_app");
                break;
        }
    }
}
