package org.example.string;

import java.util.Arrays;

public class ChaineCaractere {

    ChaineCaractere ch = new ChaineCaractere();
    ChaineCaractere ch2 = new ChaineCaractere();

    int o = ch2.id;

    static int count = 1;
    public int id = count++;
    public static void getMethodString() {
        System.out.println("----Les chaînes de caractères----");
        System.out.println();

        String s = " hello ";

        System.out.println("s.length() " + s.length()); //7
        System.out.println("s.contains(\"he\") " + s.contains("he")); //true
        System.out.println("s.isEmpty() " + s.isEmpty()); //false
        System.out.println("upp " + s.toUpperCase()); // HELLO
        System.out.println("s.startsWith(\"h\") " + s.startsWith("h")); //false
        System.out.println("s.endsWith(\" \") " + s.endsWith(" ")); //true
        System.out.println("s.replace(\"ll\", \"LL\") " + s.replace("ll", "LL")); // " heLLo "
        System.out.println("s.trim() " + s.trim()); //hello
        System.out.println("s.substring(0,3) " + s.substring(0, 3)); //" he"

        System.out.println("Array.toString(s.getBytes()) " + Arrays.toString(s.getBytes()));
        System.out.println("Array.toString(s.toCharArray()) " + Arrays.toString(s.toCharArray()));
        System.out.println("s.charAt(1) " + s.charAt(1));
        System.out.println("Array.toString(s.split(\"e\") " + Arrays.toString(s.split("e"))); //[h,llo]
    }

    static String firstName2;
    public static void getComparaisonString()
    {
        System.out.println("----Comparaison String----");
        System.out.println();

        String s = " hello ";
        String s2 = " hello ";

        System.out.println(" s == s2 " + s == s2);

        String s3 = new String(" hello ");

        System.out.println("s == s3" + s==s3); //false
        System.out.println("s.equals(s3 " + s.equals(s3)); //true

        String firstName = "Abou";
        firstName2 = "abou";

        System.out.println("firstName.equals(firstName2) " + firstName.equals(firstName2)); //false
        System.out.println("firstName.equalsIgnoreCase(firstName2) " + firstName.equalsIgnoreCase(firstName2)); //true
    }

    public static void  getFormation()
    {
        System.out.println("----Formattage String----");
        System.out.println();

        String firstName = "Abou";
        String phrase = "Salut cher %s!, Good %s!";
        String morning = "morning";
        String evening = "evening";
        String afternoon = "afternoon";

        String nouvellePhrase = String.format(phrase, firstName, evening);
        System.out.println(nouvellePhrase);

        System.out.printf("Vous allez gagner %d à l'euromillion dans %d jours", 1000000, 10);
    }
}
