package org.example.variable;

public class Variable {

    public static void getVariable() {
        //pas le mm emplacement memoire allouée
        byte b = 1;
        short s;
        s = 2;
        int i = 3;
        long l = 4;

        char c = 'c';
        boolean bool = true;

        float f = 1.25F;
        double d = 3.548746;
        long l2 = 2000000000000000000L;


        int i3 = s;
        char c2 = 100;

        System.out.println(c2);

        double d2 = i;
        d2 = l;

        b = (byte) i3;

        System.out.println("i3 " + i3 + " b i3 " + b);

        byte b2 = (byte) 128;

        //Caster perte de précision au lieu de 128 => -128

        System.out.println(b2);

        long number = 499_999_999_654_000_001L;

        double converted = (double)number;
    }
}
