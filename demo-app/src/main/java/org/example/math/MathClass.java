package org.example.math;

public class MathClass {
    public static void getMath()
    {
        System.out.println(Math.PI);
        System.out.println(Math.max(4, 7));
        System.out.println(Math.min(3, 8));
        System.out.println(Math.sqrt(4));
        int abso = Math.abs(-78);
        System.out.println(abso);

        System.out.println(Math.sqrt(-1)); //NAN
        System.out.println(0/0.0); //NAN

        System.out.println(Math.random());
        System.out.println(Math.random()*100);
        System.out.println((Math.random()*100)+100);

    }
}
