package org.example.poo.package2;

import org.example.poo.package1.ClasseC;

public class Demo {
    public static void main(){
        ClasseB classeB = new ClasseB();
        ClasseC classeC = new ClasseC();

        classeB.doSomethingB();
        classeB.doSomethingB2();
        classeB.doSomethingB3();

        classeC.doSomethingB();
        classeC.doSomethingB2();

        //doSomethingB3 => pas utilisable depuis classC car pas mm package ( default => package-private )
    }
}
