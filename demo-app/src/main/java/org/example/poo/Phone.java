package org.example.poo;

public class Phone extends Product {

    int price;

    public Phone(){

    }

    public Phone(int price) {
        this.price = price;
    }

    public Phone(int id, String name, int price)
    {
        super(id, name);
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public void whatIAm() {
        //affichage fonction mère
        super.whatIAm();
        System.out.println("Je suis un phone");
    }



    @Override
    public String toString() {
        return "Phone{" +
                "price=" + price +
                ", id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
