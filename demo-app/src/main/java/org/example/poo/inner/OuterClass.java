package org.example.poo.inner;

public class OuterClass {
    private static String message = " depuis class externe";

    public static class OtherInnerClass {

        public void afficheMessage(){
            System.out.println("Je suis une classe interne " + message);
        }
    }

    public class InnerClass {
        public void afficheMessage2(){
            System.out.println("Je suis une classe interne non statique " + message);
        }
    }
}


