package org.example.structure;

public class Structure2 {
    public static void getBoucleFor(){

        int[] tab = {1, 2, 3};
        int sum = 0;

        for (int i = 0; i < 5; i++) {
            System.out.println("i " + i);
        }
        for (int i = 0, j = 10; i<j; i++, j--) {
            System.out.println("counters i " + i + " j " + j);
        }

        for (int number : tab ) {
            sum += number;
        }

        System.out.println(sum);
    }

    public static void getBreakAndContinue(){
        System.out.println("---- Continue ----");

        for (int i = 0; i < 5 ; i++) {
            if (i%2 == 0){
                System.out.println("It's continue");
                continue;
            }
            System.out.println("counter : " + i); // 1, 3 => continue il remonte, incrémente directement sans sortir du if
        }

        for (int i = 0; i <5 ; i++) {
            for (int j = 0; j <5 ; j++) {
                if(j == 3)
                {
                    break;
                }
                System.out.println("j " + j);
            }
            System.out.println("counter " + i);
        }
    }
}
