package org.example.structure;

public class Structure {
    static int montant = 100;
    static int montant1 = 200;

    public static void getIfElse()
    {
        if(montant> montant1)
        {
            System.out.println("Montant supérieur au montant1");
        } else {
            System.out.println("Montant1 supérieur au montant");
        }
    }

    public static void getIfElseWithOutBraces(){
        if(montant > montant1)
            System.out.println("Montant supérieur au montant1");
        else
            System.out.println("Montant1 supérieur au montant");
    }

    public static void getIfElseIfElse(){
        if(montant > montant1)
            System.out.println("Montant supérieur au montant1");
        else if(montant1 > montant)
            System.out.println("Montant1 supérieur au montant");
        else
            System.out.println("Les montants sont égaux");
    }

    public static void getSwitch()
    {
        String val= "titi";

        switch (val)
        {
            case "titi" :
                System.out.println("C'est Titi");
                break;
            case "tata":
                System.out.println("C'est Tata");
                break;
            case "tutu":
                System.out.println("C'est Tutu");
                break;
            default:
                System.out.println("Who are u?");
        }
    }
}
