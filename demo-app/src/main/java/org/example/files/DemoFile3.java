package org.example.files;

import java.io.File;

public class DemoFile3 {
    public static void main()
    {
        String path = "C:\\Users\\x_mar\\Documents\\JAVA\\demo-app\\src\\main\\java\\org\\example\\files\\dossier";

        File file = new File(path);

        for (File f: file.listFiles() ) {

            parcourir(f);
        }
    }

    public static void parcourir(File f)
    {
        System.out.println(f.getName());
        System.out.println("Est-ce un directory ? " + f.isDirectory());
        System.out.println("Taille du fichier en bytes" + f.length());

        if(f.isDirectory())
        {
            for (File f1: f.listFiles())
            {
                parcourir(f1);
            }
        }
    }
}
