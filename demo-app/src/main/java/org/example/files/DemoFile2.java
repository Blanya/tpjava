package org.example.files;

import java.io.File;

public class DemoFile2 {

    private static String path1 = "C:\\Users\\x_mar\\Documents\\JAVA\\demo-app\\src\\main\\java\\org\\example\\files";

    public static void main(){
        File file = new File(path1);

        if(file.exists()){
            System.out.println("Directory " + file.isDirectory());
            System.out.println("File name " + file.getName());
            System.out.println("Absolute path " + file.getAbsolutePath());
            System.out.println("Writable " + file.canWrite());
            System.out.println("Can read " + file.canRead());
            System.out.println("Size of file en bytes " + file.length());
        }
        else {
            System.out.println("File n'existe pas");
        }

        for (File f: file.listFiles()) {
            System.out.println(f.getName());
        }
    }
}
