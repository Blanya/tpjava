package org.example;

import org.example.collections.entities.Product;
import org.example.collections.list.LinkedListDemo;
import org.example.collections.map.MapDemo;
import org.example.collections.set.SetDemo;
import org.example.files.DemoFile;
import org.example.files.DemoFile2;
import org.example.files.DemoFile3;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
//import org.example.readFromConsole.ReadFromConsole;
//import org.example.string.ChaineCaractere;
//import org.example.string.ChaineCaractere1;
//import org.example.structure.Structure2;

public class Main {
//    méthode attachée à ma classe
    public static void main(String[] args) throws IOException {
//        Variable.getVariable();
//        ReadFromConsole.getReadWriteConsole();
//        ChaineCaractere.getMethodString();
//        ChaineCaractere.getComparaisonString();
//        ChaineCaractere.getFormation();
//        ChaineCaractere1.getRegex();
//        Structure2.getBoucleFor();
//        Operators.getOperationAndComparaisonType();
//        ArrayClass.getArray();
//        Structure2.getBreakAndContinue();
//        int i = 10;
//        i = MethodParam.changeInt(i);
//        System.out.println(i);
//
//        int[] array = {1,2,3};
//
//        //fonctionne car par référence comme pour les objets
//        MethodParam.changeArray(array);
//        System.out.println("Array après méthode changeArray" + Arrays.toString(array));
//
//        System.out.println("class :" + array.getClass());

//        DemoEnum.getEnum();
//        Product p = new Product();
//        Product p2 = new Product(1, "T-Shirt");
//
//        System.out.println(p.toString());
//        System.out.println(p2.toString());
//
//        Phone phone = new Phone(12);
//
//        phone.setPrice(15);
//        phone.setName("Xiaomi");
//        phone.setId(4);
//
//        System.out.println(phone.toString());
//
//        Phone phone2 = new Phone(1, "oppo", 124);
//        System.out.println(phone2.toString());
//
//        Product.getBonjour();
//
//        phone.whatIAm();

//        OuterClass outerClass = new OuterClass();
//
//        //non static doit etre instanciée
//
//        OuterClass.InnerClass innerClass = outerClass.new InnerClass();
//        innerClass.afficheMessage2();
//
//        OuterClass.OtherInnerClass otherInnerClass = new OuterClass.OtherInnerClass();
//        otherInnerClass.afficheMessage();

//        ListDemo.main();

//        List<RandomPerson> personList = new ArrayList<>();
//
//        RandomPerson.addDate(5, personList);
//
//        ListDemo.triOrderRandomPerson(Order.CITY, personList);
//
//        RandomPerson.affichePerson(personList);

//        IteratorDemo.main();

//        LinkedListDemo.main();

//        SetDemo.demo();

//        MapDemo.main();

//        DemoFile.main3();

//        DemoFile2.main();

        DemoFile3.main();
    }


}