package org.example.collections.set;

import org.example.collections.entities.User;

import java.util.HashSet;
import java.util.Set;

public class SetDemo {
    public static void demo(){

        Set<Integer> set = new HashSet<>();

        set.add(1);
        set.add(3);
        set.add(1);
        set.add(4);

        System.out.println(set);


        Set<User> users = new HashSet<>();

        //deux objets différents car 2 références => emplacements différents
        User user1 = new User("Joel", "Smith", "1234", "joel@mail.fr");
        User user2 = new User("Joel", "Smith", "1234", "joel@mail.fr");

        user2.setId(1);

        users.add(user1);
        users.add(user2);

        System.out.println(user1.hashCode());
        System.out.println(user2.hashCode());

        User user3 = new User("Joel", "Smith", "1234", "joel@mail.fr");
        User user4 = new User("Mike", "Smith", "1234", "mike@mail.fr");

        user3 = user4;

        System.out.println(user3.hashCode());
        System.out.println(user4.hashCode());

        users.add(user3);
        users.add(user4);

        for (User u : users) {
            System.out.println("user " + u);
        }
    }
}
