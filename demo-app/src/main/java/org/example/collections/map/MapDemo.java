package org.example.collections.map;

import org.example.collections.entities.Product;
import org.example.collections.entities.User;

import java.util.HashMap;
import java.util.Map;

public class MapDemo {
    public static void main()
    {
        Map<Integer, String> map = new HashMap<>(); //Integer = key, String= valeur

        map.put(2, "Maison");
        map.put(3, "HLM");
        map.put(5, "Caravane");
        map.put(6, "Chalet");

        System.out.println("Get un element de la map " + map.get(6));

        System.out.println("Iteration ds la map pour récupérer les keys :");

        for (Integer key: map.keySet() ) {
            System.out.println("key " + key);
        }

        System.out.println("Recuperation des valeurs: ");

        for (String value: map.values() ) {
            System.out.println(value);
        }

        System.out.println("Recuperation key valeur");

        for (Map.Entry<Integer, String > entry : map.entrySet()) {
            System.out.println("Entry key " + entry.getKey() + " , Value " + entry.getValue());
        }

        Map<User, Product> userProductMap = new HashMap<>(); //key unique

        User user = new User();
        user.setFirstName("Eddy");
        user.setLastName("Mitchell");

        Product product = new Product();

        userProductMap.put(user, product);

        System.out.println("Get product by user " + userProductMap.get(user));
    }
}
