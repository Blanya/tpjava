package org.example.collections.entities;

import net.datafaker.Faker;

import java.util.Collection;
import java.util.Collections;

import static java.lang.Integer.parseInt;

public class Product {
    private String id;
    private String productName;
    private String categoryName;
    private float price;

    public Product(){
        Faker faker = new Faker();
        this.id = faker.code().ean8();
        this.productName = faker.commerce().productName();
        this.categoryName = faker.commerce().department();
        this.price = Float.parseFloat((faker.commerce().price()));
    }

    public Product(String id, String productName, String categoryName, Integer price) {
        this.id = id;
        this.productName = productName;
        this.categoryName = categoryName;
        this.price = price;
    }

    public static void addProducts(int taille, Collection<Product> list)
    {
        for (int i = 0; i < taille ; i++) {
            Product product = new Product();
            list.add(product);
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id='" + id + '\'' +
                ", productName='" + productName + '\'' +
                ", categoryName='" + categoryName + '\'' +
                ", price=" + price +
                '}';
    }
}
