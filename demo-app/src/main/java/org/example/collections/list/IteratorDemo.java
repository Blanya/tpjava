package org.example.collections.list;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

public class IteratorDemo {
    public static void main(){
        ArrayList<Integer> list = new ArrayList<>();

        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);

        Iterator iterator = list.iterator();
        //iterator
        while(iterator.hasNext()){
            System.out.println(iterator.next() + " ");
        }

        //listIterator
        ListIterator listIterator = list.listIterator();

        while (listIterator.hasNext())
        {
            System.out.println(listIterator.next()  );
        }

        //sens inverse

        while (listIterator.hasPrevious()){
            System.out.println(listIterator.previous() + " ");
        }
    }
}
