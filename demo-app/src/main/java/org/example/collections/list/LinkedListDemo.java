package org.example.collections.list;

import org.example.collections.list.entity.RandomPerson;

import java.util.LinkedList;

import static org.example.collections.list.entity.RandomPerson.addDate;
import static org.example.collections.list.entity.RandomPerson.affichePerson;

public class LinkedListDemo {

    public static LinkedList<RandomPerson> listDemo = new LinkedList<>();

    public static void main(){
        System.out.println("Demo sur linkedList");

        addDate(5, listDemo);
        affichePerson(listDemo);

        RandomPerson person = new RandomPerson();
        person.firstName = "Patricia";

        listDemo.addFirst(person);

        RandomPerson person1 = new RandomPerson();
        person1.firstName = "Louis";

        listDemo.addLast(person1);

//        listDemo.removeFirst();

        RandomPerson person2 = new RandomPerson();
        person2 = person1;

        listDemo.addFirst(person2);
        affichePerson(listDemo);

        listDemo.removeFirstOccurrence(person1);
        listDemo.removeLastOccurrence(person1);

        listDemo.getFirst();
        listDemo.getLast();

        System.out.println("Premier element de ma liste " + listDemo.getFirst());
        System.out.println("Dernier element de ma liste " + listDemo.getLast());
    }
}
