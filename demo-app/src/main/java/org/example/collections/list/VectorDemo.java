package org.example.collections.list;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

public class VectorDemo {
    public static void demo(){
        ArrayList<String> list = new ArrayList<>();

        list.add("Toto");
        list.add("Titi");
        list.add("Tata");
        list.add("Tutu");

        Iterator<String> it = list.iterator();

        while (it.hasNext()){
            System.out.println("nom : " + it.next());
        }

        Vector<String> vector = new Vector<>();

        vector.addElement("Marco");
        vector.addElement("Paulo");
        vector.addElement("Michel");

        //specifiq vector

        Enumeration<String> e = vector.elements();

        while (e.hasMoreElements())
        {
            System.out.println("Element " + e.nextElement());
        }
    }
}
