package org.example.collections.list;

import org.example.collections.list.entity.RandomPerson;
import org.example.collections.list.enums.Order;
import org.example.collections.list.tools.FlexibleRandomComparator;

import java.util.*;

import static org.example.collections.list.entity.RandomPerson.affichePerson;
import static org.example.collections.list.entity.RandomPerson.addDate;

public class ListDemo {

    public static List<RandomPerson> listeP = new ArrayList<>();
    public static List<RandomPerson> listDemo = new ArrayList<>();
    public static List<RandomPerson> listDemo3 = new ArrayList<>();
    public static void main()
    {
        List<String> liste = new ArrayList<>();
        liste.add("Michel");
        liste.add("Toto");
        liste.add("Paul");
        liste.add("Marco");

        String nom = liste.get(1);
        Object obj = liste.get(1);
        System.out.println(liste);

        //Retient ce qu'il y avait avant et ce qu'il y a après à l'emplacement
        List<String> list2 = new LinkedList<>(liste); //Add all

        List<Integer> list3 = new ArrayList<>(Arrays.asList(2, 5, 67, 45, 32, 1));

        Collections.sort(list3);

        for (Integer i: list3 ) {
            System.out.println(i);
        }

        int[] tab = new int[]{1, 4, 0, 8, 9, 2, 3};
        Arrays.sort(tab);

        System.out.println(list2);
        list2.clear(); //vider

        //Demo avec faker => ArrayList obj

        RandomPerson.addDate(3, listeP);
        affichePerson(listeP);

        RandomPerson person = new RandomPerson();
        person.firstName = "MichMich";

        System.out.println("--Ajout d'une personne--");

        listeP.add(3, person);

        RandomPerson person1 = new RandomPerson();
        person1.firstName = "Lucie";

        listeP.add(listeP.indexOf(person), person1);
        //ne supprime pas la personne index 3 mais la déplace
        affichePerson(listeP);

        System.out.println("----Création d'une deuxième liste----");

        addDate(7, listDemo);
        affichePerson(listDemo);

        System.out.println("Add list2 a la première");

        listeP.addAll(listDemo);

        affichePerson(listeP);

        System.out.println("Supprimer un élément de la liste");

        listeP.remove(person);
        listeP.remove(3);

        affichePerson(listeP);

        System.out.println("listP contient la liste demo2 : " + listeP.containsAll(listDemo));

        listeP.remove(9);

        System.out.println(listeP.containsAll(listDemo));

        System.out.println("sous liste demo de l'index 3 à 9");

        listDemo3 = listeP.subList(3, 9);

        affichePerson(listDemo3);
    }

    public static void triOrderRandomPerson(Order order, List<RandomPerson> liste){
        FlexibleRandomComparator comparator = new FlexibleRandomComparator();
        comparator.setOrder(order);
        liste.sort(comparator); // == Collections.sort(liste, comparator)
    }
}
