package org.example.palindrome;

import java.util.Scanner;

public class Palindrome {
    public static void checkIfPalindrome()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Entrer un mot ");
        String word = sc.nextLine();
        word = word.toLowerCase();

        int i = 0;
        int longueur = word.length()-1;
        boolean same = true;

        while(i < longueur/2 && same)
        {
            same = word.charAt(i) == word.charAt(longueur - i);
            i++;
        }

        if(same)
            System.out.println("Le mot " + word + " est un palindrome");
        else
            System.out.println("Le mot " + word + " n'est pas un palindrome");
    }
}
