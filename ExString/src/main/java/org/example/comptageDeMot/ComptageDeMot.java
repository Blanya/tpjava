package org.example.comptageDeMot;

import java.util.Scanner;

public class ComptageDeMot {

    public static void getNbMots()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Donnez moi une phrase : ");

        String sentence = sc.nextLine();
        // \\s+ divise la chaîne donnée autour d’un espace blanc
        String[] words = sentence.split("\\s+");

        System.out.println("La phrase " + sentence + " contient " + words.length + " mots");
    }
}
