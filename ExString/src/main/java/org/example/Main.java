package org.example;

import org.example.anagramme.Anagramme;
import org.example.comptageDeMot.ComptageDeMot;
import org.example.occurence.Occurence;
import org.example.palindrome.Palindrome;
import org.example.pyramide.Pyramide;

public class Main {
    public static void main(String[] args) {


//        ComptageDeMot.getNbMots();
//        Occurence.getOccurenceLetter();
//        Anagramme.askWordAndCheckAnagramme();
        Palindrome.checkIfPalindrome();
       Pyramide.getPyramide();
    }
}