package org.example.occurence;

import java.util.Arrays;
import java.util.Scanner;

public class Occurence {

    public static void getOccurenceLetter()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Donnez moi un mot ");

        String word = sc.next();
        word = word.toLowerCase();

        System.out.println("Donnez moi la lettre pour connaître le nombre de fois qu'elle apparaît dans le mot ");
        char letter = sc.next().toLowerCase().charAt(0);


        int nb = 0;
        for (int i = 0; i < word.length(); i++) {
            if(word.charAt(i) == letter)
                nb++;
        }

        System.out.printf("Il y a %d fois la lettre %s dans le mot %s", nb, letter, word);

    }
}
