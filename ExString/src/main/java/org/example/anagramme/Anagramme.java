package org.example.anagramme;

import java.util.Arrays;
import java.util.Scanner;

public class Anagramme {

    public static void askWordAndCheckAnagramme()
    {
        Scanner sc = new Scanner(System.in);

        System.out.println("Donnez moi un mot");
        String fstWord = sc.next();

        System.out.println("Donnez moi un autre mot");
        String scdWord = sc.next();

        checkIfAnagramme(fstWord, scdWord);
    }

    public static void checkIfAnagramme(String word, String otherWord)
    {
        word = word.toLowerCase();
        otherWord = otherWord.toLowerCase();

        if(word.length() == otherWord.length())
        {
            char[] tab1 = word.toCharArray();
            char[] tab2 = otherWord.toCharArray();

            Arrays.sort(tab1);
            Arrays.sort(tab2);

            String newWord = new String(tab1);
            String newOtherWord = new String(tab2);

            if(newWord.equals(newOtherWord))
                System.out.printf("Les deux mots %s et %s sont des anagrammes", word, otherWord);
            else
                System.out.printf("Les deux mots %s et %s ne sont pas des anagrammes", word, otherWord
                );

        }
        else
            System.out.printf("Les deux mots %s et %s ne sont pas des anagrammes", word, otherWord
            );

    }
}
