package org.example.pyramide;

import java.util.Scanner;

public class Pyramide {
    public static void getPyramide()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Donnez moi la hauteur de la pyramide");
        int count = sc.nextInt();

        for (int i = 1; i < count+1 ; i++) {
            //Le caractère de code ASCII 0, appelé NUL. variable caractère initialisée à 0
            String result = new String(new char[i]).replace("\0", "* ");
            System.out.println(result);
        }
        for (int i = count-1; i >0 ; i--) {
            String result = new String(new char[i]).replace("\0", "* ");
            System.out.println(result);
        }
    }
}
