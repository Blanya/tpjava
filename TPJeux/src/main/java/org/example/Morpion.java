package org.example;

import java.util.Arrays;
import java.util.Objects;
import java.util.Scanner;

public class Morpion {
    static String[][] tableauMorpion = {{" * "," * ", " * "}, {" * "," * ", " * "}, {" * "," * ", " * "}};
    static String joueur1 = "Joueur 1";
    static String joueur2 = "Joueur 2";
    static int win = 0;
    static Scanner sc = new Scanner(System.in);
    public static void jouer(String joueur, String symbole)
    {
        //demande case où jouer
        System.out.println(joueur + ", vous désirez jouer sur quelle ligne ? 1, 2 ou 3 ? ");
        int ligne = sc.nextInt();
        System.out.println(joueur + ", vous désirez jouer sur quelle colonne ? 1, 2 ou 3 ? ");
        int colonne = sc.nextInt();

        if (!Objects.equals(tableauMorpion[ligne - 1][colonne - 1], " * "))
        {
            System.out.println("case déjà prise ou saisie incorrecte, une autre place ?");
            jouer(joueur, symbole);
        }
        else
        {
            tableauMorpion[ligne-1][colonne-1] = symbole;
            affichageTableau();
            verifCells(tableauMorpion, joueur);

            switch (win)
            {
                case 1:
                    getWinner(joueur);
                    break;
                case 2:
                    getWinner("Joueur 1 & 2");
                    break;
                default:
                    if(Objects.equals(joueur, joueur1))
                    {
                        jouer(joueur2, " o ");
                    }
                    else
                    {
                        jouer(joueur1, " x ");
                    }
                    break;
            }
        }
    }
    public static void verifCells(String[][] game, String joueur)
    {
        getSymbol(game[0][0], game[0][1], game[0][2], joueur);
        getSymbol(game[1][0], game[1][1], game[1][2], joueur);
        getSymbol(game[2][0], game[2][1], game[2][2], joueur);
        getSymbol(game[0][0], game[1][0], game[2][0], joueur);
        getSymbol(game[0][1], game[1][1], game[2][1], joueur);
        getSymbol(game[0][2], game[1][2], game[2][2], joueur);
        getSymbol(game[0][0], game[1][1], game[2][2], joueur);
        getSymbol(game[0][2], game[1][1], game[2][0], joueur);
    }

    public static void getSymbol(String firstCell, String secondCell, String thirdCell, String joueur)
    {
        if (!Objects.equals(firstCell, " * ") && //Si la 1ère cellule n'est pas null -> Retourne 1 ou 2
                Objects.equals(firstCell, secondCell) && //et que la 2ème cellule est égale à la 1ère
                Objects.equals(firstCell, thirdCell))//et si la 3eme cellule est égale à la 1ère
        {
            win = 1;
        }
        //parcourir tableau
        int nul = 0;
        for (String[] strings : tableauMorpion) {
            for (int j = 0; j < strings.length; j++) {
                if (!Objects.equals(strings[j], " * ")) {
                    nul += 1;
                }
            }
        }
        if(nul == 9)
        {
            win = 2;
        }
    }

    static int reponse;
    public static void getWinner(String joueur) {
        if(Objects.equals(joueur, "Joueur 1 & 2"))
        {
            System.out.println("Le jeu est fini, match nul " + joueur + " ! Une autre partie? 1- Oui 2-Non : " );
            reponse = sc.nextInt();
        }
        else
        {
            System.out.println("Le jeu est fini, bravo " + joueur + " ! Tu as gagné ! Une autre partie? 1- Oui 2-Non : " );
            reponse = sc.nextInt();
        }

        if(reponse == 1)
        {
            tableauMorpion = new String[][]{
                    {" * ", " * ", " * "},
                    {" * ", " * ", " * "},
                    {" * ", " * ", " * "}
            };
            win = 0;
            jouer(joueur1, " x ");
        }
        else
        {
            System.out.println("Fin du jeu");
        }
    }

    public static void affichageTableau()
    {
        for (String[] strings : tableauMorpion) {
            System.out.println(Arrays.toString(strings));
        }
    }
}
